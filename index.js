const tmi = require ("tmi.js");
const config = require("./global/config.json");
const fs = require("fs");

const options = {
  options: {
    debug: true
  },
  connection: {
    cluster: "aws",
    reconnect: true
  },
  identity: {
    username: config.username,
    password: config.password
  },
  channels: config.channels
}

const bot = new tmi.client(options);

bot.connect();

const bot_commands = new Map();

fs.readdir("./commands", (error, files) => {
  if (error) return console.log(error);
  const file_commands = files.filter(file => file.split(".").pop() === "js");
  if (file_commands.length <= 0) return console.log("No commands files detected.");
  file_commands.forEach((file_name, file_num) => {
    const commands = require(`./commands/${file_name}`);
    console.log(`Loaded command file [${++file_num}] ${file_name}.`);
    config.cmd.list.push(file_name);
    bot_commands.set(commands.help.name, commands);
  }
  console.log(`Total command files loaded: ${file_commands.length}.\n`);
}

bot.on("chat", (channel, user, message, self) => {
  
  if (self || !message.startsWith(config.prefix) return;
  
  const msg_array = message.toLowerCase().split(" ");
  const prefix = config.prefix;
  const command = msg_array[1];
  const args = msg_array.slice(1);
  const command_files = bot_commands.get(command.slice(config.prefix));
  
  if (command_files) return command_file.run(config, bot, user, message, args);
  
}
