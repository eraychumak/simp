module.exports.run = (config, bot, user, message, args) => {
  
  const msg_send = (content) => {return bot.say(config.channel, content)};
  
  args[0] ? msg_send("") : msg_send(config.cmd.list.map(cmd => `~${cmd.replace(".js", "");}`).join(" | "));
  
}

module.exports.help = {
  name: "help"
}
